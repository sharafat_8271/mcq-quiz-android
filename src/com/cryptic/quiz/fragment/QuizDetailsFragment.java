package com.cryptic.quiz.fragment;

import android.app.Application;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.ScrollView;
import android.widget.TextView;
import com.cryptic.android.utils.StringUtils;
import com.cryptic.android.utils.view.fragment.CrypticDetailsFragment;
import com.cryptic.quiz.R;
import com.cryptic.quiz.activity.QuizSectionListActivity;
import com.cryptic.quiz.controller.QuizController;
import com.cryptic.quiz.domain.Quiz;
import com.cryptic.quiz.service.QuizSessionManager;
import com.cryptic.quiz.util.Utils;
import com.google.inject.Inject;
import roboguice.inject.InjectView;

/**
 * @author sharafat
 */
public class QuizDetailsFragment extends CrypticDetailsFragment {
    public static final String KEY_QUIZ_MODE = "quiz_mode";

    @Inject
    private Application application;
    @Inject
    private QuizController quizController;
    @Inject
    private QuizSessionManager quizSessionManager;

    @InjectView(R.id.scroll_view)
    private ScrollView scrollView;
    @InjectView(R.id.quiz_title)
    private TextView quizTitleTextView;
    @InjectView(R.id.quiz_details)
    private TextView quizDetailsTextView;
    @InjectView(R.id.no_of_questions)
    private TextView noOfQuestionsTextView;
    @InjectView(R.id.exam_time)
    private TextView examTimeTextView;
    @InjectView(R.id.learning_mode)
    private RadioButton learningModeOption;
    @InjectView(R.id.exam_mode)
    private RadioButton examModeOption;
    @InjectView(R.id.start_exam_button)
    private Button startExamButton;

    private Quiz quiz;

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        startExamButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startExamButtonClicked();
            }
        });
    }

    private void startExamButtonClicked() {
        int quizMode = QuizSessionManager.QUIZ_MODE_LEARNING;

        if (learningModeOption.isChecked()) {
            quizMode = QuizSessionManager.QUIZ_MODE_LEARNING;
        } else if (examModeOption.isChecked()) {
            quizMode = QuizSessionManager.QUIZ_MODE_EXAM;
        }

        quizSessionManager.prepare(quiz, quizMode);

        startActivity(new Intent(application, QuizSectionListActivity.class));
    }

    @Override
    public void setItem(Object item) {
        quiz = (Quiz) item;
    }

    @Override
    protected int detailsViewResource() {
        return R.layout.quiz_details;
    }

    @Override
    protected Class homeActivity() {
        return Utils.getHomeActivity();
    }

    @Override
    protected void updateView() {
        if (quiz != null) {
            quizTitleTextView.setText(quiz.getTitle());

            if (!StringUtils.isEmpty(quiz.getDetails())) {
                quizDetailsTextView.setText(quiz.getDetails());
                quizDetailsTextView.setVisibility(View.VISIBLE);
            } else {
                quizDetailsTextView.setVisibility(View.GONE);
            }

            noOfQuestionsTextView.setText(Integer.toString(quizController.getNoOfQuestionsInQuiz(quiz)));
            examTimeTextView.setText(quiz.getExamTime() != null ? quiz.getExamTime().format("c:m:s") : "--:--:--");

            scrollView.setVisibility(View.VISIBLE);
        } else {
            scrollView.setVisibility(View.GONE);
        }
    }
}
