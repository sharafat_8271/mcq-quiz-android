package com.cryptic.quiz.fragment;

import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import com.cryptic.android.utils.view.SingleLineListItemView;
import com.cryptic.android.utils.view.TwoLineListItemWithImageView;
import com.cryptic.android.utils.view.fragment.CrypticDetailsFragment;
import com.cryptic.android.utils.view.fragment.CrypticListFragment;
import com.cryptic.quiz.R;
import com.cryptic.quiz.activity.QuizActivity;
import com.cryptic.quiz.domain.Section;
import com.cryptic.quiz.service.QuizSessionManager;
import com.google.inject.Inject;
import roboguice.inject.InjectFragment;
import roboguice.inject.InjectResource;

import javax.annotation.Nullable;

/**
 * @author sharafat
 */
public class QuizSectionListFragment extends CrypticListFragment {
    @Inject
    private QuizSessionManager quizSessionManager;

    @InjectFragment(R.id.details_container)
    @Nullable
    private QuizFragment quizFragment;

    @InjectResource(R.string.answered_m_out_of_n_questions)
    private String answeredMOutOfNQuestions;

    @Override
    public void onResume() {
        super.onResume();
        updateNoOfQuestionsAnswered();
    }

    public void updateNoOfQuestionsAnswered() {
        if (quizSessionManager.getQuizMode() == QuizSessionManager.QUIZ_MODE_EXAM) {
            ((QuizSectionsListAdapter) getListAdapter()).notifyDataSetChanged();
        }
    }

    @Override
    protected ListAdapter listAdapter() {
        return new QuizSectionsListAdapter();
    }

    @Override
    protected Class detailsActivity() {
        return QuizActivity.class;
    }

    @Override
    protected CrypticDetailsFragment detailsFragment() {
        return quizFragment;
    }

    public void selectNextQuizSection() {
        assert dualPane;
        int nextQuizSectionPosition = (getCurrentlySelectedQuizSection() + 1) % getListAdapter().getCount();
        selectItemInList(nextQuizSectionPosition);
    }

    private int getCurrentlySelectedQuizSection() {
        return quizSessionManager.listQuizSections().indexOf(quizSessionManager.getActiveSection());
    }


    private class QuizSectionsListAdapter extends ArrayAdapter<Section> {
        public QuizSectionsListAdapter() {
            super(application, 0);

            for (Section section : quizSessionManager.listQuizSections()) {
                add(section);
            }
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            Section section = getItem(position);

            String primaryText = section.getHeading();

            switch (quizSessionManager.getQuizMode()) {
                case QuizSessionManager.QUIZ_MODE_LEARNING:
                    return SingleLineListItemView.getView(layoutInflater, R.layout.simple_list_item_1_medium_font,
                            android.R.layout.simple_list_item_activated_1, convertView, parent, primaryText);
                case QuizSessionManager.QUIZ_MODE_EXAM:
                    String secondaryText = String.format(answeredMOutOfNQuestions,
                            quizSessionManager.getNoOfQuestionsAnsweredInSection(section),
                            quizSessionManager.getNoOfQuestionsInSection(section));
                    int percentageCompletedImageRes = quizSessionManager.getImageOfPercentageCompleted(section);

                    return TwoLineListItemWithImageView.getView(layoutInflater, convertView, parent, primaryText,
                            secondaryText, percentageCompletedImageRes);
                default:
                    return null;
            }
        }
    }
}
