package com.cryptic.quiz.fragment;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.cryptic.android.utils.StringUtils;
import com.cryptic.android.utils.photo.util.ImageUtils;
import com.cryptic.android.utils.view.fragment.CrypticDetailsFragment;
import com.cryptic.quiz.R;
import com.cryptic.quiz.domain.Answer;
import com.cryptic.quiz.domain.Question;
import com.cryptic.quiz.domain.Section;
import com.cryptic.quiz.service.QuizSessionManager;
import com.google.inject.Inject;
import roboguice.inject.InjectFragment;
import roboguice.inject.InjectResource;
import roboguice.inject.InjectView;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author sharafat
 */
public class QuizFragment extends CrypticDetailsFragment {
    public static final int RESULT_FINISH = 2010;   //the year I "finish"ed my graduation :D

    @Inject
    private Activity context;
    @Inject
    private QuizSessionManager quizManager;

    @InjectFragment(R.id.list_container)
    @Nullable
    private QuizSectionListFragment quizSectionListFragment;

    @InjectView(R.id.scroll_view)
    private ScrollView scrollView;
    @InjectView(R.id.quiz_title)
    private TextView quizTitleTextView;
    @InjectView(R.id.exam_time)
    private TextView examTimeTextView;
    @InjectView(R.id.question_no)
    private TextView questionNoTextView;
    @InjectView(R.id.question_text)
    private TextView questionTextTextView;
    @InjectView(R.id.answer_container)
    private LinearLayout answerContainer;
    @InjectView(R.id.explanation_container)
    private LinearLayout explanationContainer;
    @InjectView(R.id.explanation)
    private TextView explanationTextView;
    @InjectView(R.id.prev_btn)
    private Button previousButton;
    @InjectView(R.id.next_btn)
    private Button nextButton;

    @InjectResource(R.string.question_m_out_of_n_in_section)
    private String questionMOutOfNInSection;
    @InjectResource(R.string.answered_m_out_of_n_questions)
    private String answeredMOutOfNQuestions;
    @InjectResource(R.string.next)
    private String next;
    @InjectResource(R.string.check_answer)
    private String checkAnswer;
    @InjectResource(R.string.finish_section)
    private String finishSection;
    @InjectResource(R.string.finish)
    private String finish;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        context = getActivity();

        previousButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                quizManager.previous();
                updateView();
            }
        });
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isExamMode() || !quizManager.isQuestionAnswered()) {
                    saveMarkedAnswers();
                }

                String nextButtonLabel = nextButton.getText().toString();
                if (nextButtonLabel.equals(checkAnswer)) {
                    highlightCorrectIncorrectAnswer();
                    showExplanationIfExists();
                    setNextButtonLabel();
                } else if (nextButtonLabel.equals(finishSection)) {
                    if (dualPanel) {  //dual pane
                        assert quizSectionListFragment != null;
                        quizSectionListFragment.selectNextQuizSection();
                    } else {
                        getActivity().finish();
                    }
                } else if (nextButtonLabel.equals(finish)) {
                    finishQuiz();
                } else {
                    quizManager.next();
                    updateView();
                }
            }
        });
    }

    @Override
    protected void updateView() {
        if (quizManager.getActiveSection() == null) {
            return;
        }

        quizTitleTextView.setText((quizManager.isSingleSetQuiz() ? ""
                : quizManager.getActiveSectionHeading() + " - ") + quizManager.getQuizTitle());
        examTimeTextView.setVisibility(isExamMode() ? View.VISIBLE : View.GONE);
        questionNoTextView.setText(String.format(questionMOutOfNInSection, quizManager.getCurrentQuestionNo(),
                quizManager.getNoOfQuestionsInActiveSection()));

        Question question = quizManager.getQuestion();
        questionTextTextView.setText(StringUtils.getNonEmptyString(question.getText(), ""));
        questionTextTextView.setCompoundDrawablesWithIntrinsicBounds(
                drawable(question.getImageLeft()), drawable(question.getImageTop()),
                drawable(question.getImageRight()), drawable(question.getImageBottom()));

        updateAnswersView();

        explanationContainer.setVisibility(View.GONE);
        if (isLearningMode() && quizManager.isQuestionAnswered()) {
            showExplanationIfExists();
        }

        previousButton.setVisibility(quizManager.isFirstQuestionInActiveSection() ? View.INVISIBLE : View.VISIBLE);
        setNextButtonLabel();

        scrollView.fullScroll(ScrollView.FOCUS_UP);
    }

    //TODO: Make code efficient by reusing layouts
    private void updateAnswersView() {
        List<Answer> answers = quizManager.getAnswers();

        LinearLayout compoundButtonContainer = quizManager.canChooseMultipleAnswers()
                ? new LinearLayout(context) : new RadioGroup(context);

        LinearLayout.LayoutParams compoundButtonContainerLayoutParams = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        compoundButtonContainer.setLayoutParams(compoundButtonContainerLayoutParams);
        compoundButtonContainer.setOrientation(LinearLayout.VERTICAL);

        LinearLayout.LayoutParams compoundButtonLayoutParams = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        compoundButtonLayoutParams.setMargins(0, 0, 0, dp2px(5));

        for (int i = 0; i < answers.size(); i++) {
            Answer answer = answers.get(i);

            CompoundButton compoundButton = quizManager.canChooseMultipleAnswers()
                    ? new CheckBox(context) : new RadioButton(context);
            compoundButton.setLayoutParams(compoundButtonLayoutParams);
            compoundButton.setPadding(dp2px(45), dp2px(5), dp2px(5), dp2px(5));
            compoundButton.setCompoundDrawablePadding(dp2px(5));
            compoundButton.setText(StringUtils.getNonEmptyString(answer.getText(), ""));
            compoundButton.setCompoundDrawablesWithIntrinsicBounds(
                    drawable(answer.getImageLeft()), drawable(answer.getImageTop()),
                    drawable(answer.getImageRight()), drawable(answer.getImageBottom()));
            compoundButton.setId(i);

            if (answer.isMarked() && quizManager.isQuestionAnswered()) {
                compoundButton.setChecked(true);
            }

            compoundButtonContainer.addView(compoundButton);
        }

        answerContainer.removeAllViews();
        answerContainer.addView(compoundButtonContainer);

        if (isLearningMode() && quizManager.isQuestionAnswered()) {
            highlightCorrectIncorrectAnswer();
        }
    }

    private boolean isLearningMode() {
        return quizManager.getQuizMode() == QuizSessionManager.QUIZ_MODE_LEARNING;
    }

    private boolean isExamMode() {
        return quizManager.getQuizMode() == QuizSessionManager.QUIZ_MODE_EXAM;
    }

    private Drawable drawable(byte[] image) {
        return ImageUtils.getDrawable(image, getResources());
    }

    private int dp2px(float dp) {
        return ImageUtils.dp2px(dp);
    }

    private void saveMarkedAnswers() {
        if (quizManager.canChooseMultipleAnswers()) {
            List<Integer> markedAnswers = new ArrayList<Integer>();
            LinearLayout compoundButtonContainer = (LinearLayout) getCompoundButtonContainer();
            for (int i = 0; i < compoundButtonContainer.getChildCount(); i++) {
                CheckBox checkBox = (CheckBox) compoundButtonContainer.getChildAt(i);
                if (checkBox.isChecked()) {
                    markedAnswers.add(checkBox.getId());
                }
            }
            quizManager.answerMarked(markedAnswers.toArray(new Integer[markedAnswers.size()]));
        } else {
            RadioGroup radioGroup = (RadioGroup) getCompoundButtonContainer();
            int checkedRadioButtonId = radioGroup.getCheckedRadioButtonId();
            if (checkedRadioButtonId != -1) {
                quizManager.answerMarked(checkedRadioButtonId);
            } else {
                quizManager.answerMarked();
            }
        }

        if (dualPanel && isExamMode()) {
            assert quizSectionListFragment != null;
            quizSectionListFragment.updateNoOfQuestionsAnswered();
        }
    }

    private View getCompoundButtonContainer() {
        return answerContainer.getChildAt(0);
    }

    private void highlightCorrectIncorrectAnswer() {
        LinearLayout compoundButtonContainer = (LinearLayout) getCompoundButtonContainer();

        Map<Integer, Boolean> answerPositionsCorrectnessMap = quizManager.getAnswerPositionsWithCorrectness();
        for (Integer answerIndex : answerPositionsCorrectnessMap.keySet()) {
            boolean answerCorrect = answerPositionsCorrectnessMap.get(answerIndex);
            int answerBackgroundColorRes = answerCorrect ? R.color.correct_answer : R.color.incorrect_answer;
            compoundButtonContainer.getChildAt(answerIndex).setBackgroundResource(answerBackgroundColorRes);
        }
    }

    private void showExplanationIfExists() {
        String explanation = quizManager.getQuestion().getExplanation();
        if (!StringUtils.isEmpty(explanation)) {
            explanationTextView.setText(explanation);
            explanationContainer.setVisibility(View.VISIBLE);
        }
    }

    private void setNextButtonLabel() {
        if (isLearningMode() && !quizManager.isQuestionAnswered()) {
            nextButton.setText(checkAnswer);
        } else if (quizManager.isLastQuestionInActiveSection()) {
            nextButton.setText(quizManager.isSingleSetQuiz() ? finish : finishSection);
        } else {
            nextButton.setText(next);
        }
    }

    private void finishQuiz() {
        if (isExamMode()) {
//            startActivity(new Intent(context, ResultActivity.class));
        }

        getActivity().setResult(RESULT_FINISH);
        getActivity().finish();
    }

    @Override
    protected int detailsViewResource() {
        return R.layout.quiz;
    }

    @Override
    public void setItem(Object item) {
        quizManager.setActiveSection((Section) item);
    }
}
