package com.cryptic.quiz.fragment;

import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import com.cryptic.android.utils.adapter.Filterable;
import com.cryptic.android.utils.adapter.FilterableArrayAdapter;
import com.cryptic.android.utils.view.fragment.CrypticDetailsFragment;
import com.cryptic.android.utils.view.fragment.CrypticListFragment;
import com.cryptic.android.utils.view.IndefinitelyProgressingTask;
import com.cryptic.android.utils.view.SingleLineListItemView;
import com.cryptic.quiz.R;
import com.cryptic.quiz.activity.QuizDetailsActivity;
import com.cryptic.quiz.controller.QuizController;
import com.cryptic.quiz.domain.Quiz;
import com.google.inject.Inject;
import roboguice.inject.InjectFragment;
import roboguice.inject.InjectResource;

import javax.annotation.Nullable;
import java.util.Comparator;
import java.util.List;

/**
 * @author sharafat
 */
public class QuizListFragment extends CrypticListFragment {
    @Inject
    private QuizController quizController;

    @InjectFragment(R.id.details_container)
    @Nullable
    private QuizDetailsFragment quizDetailsFragment;

    @InjectResource(R.string.loading_quizes)
    private String loadingQuizes;

    @Override
    protected ListAdapter listAdapter() {
        return new QuizListAdapter();
    }

    @Override
    protected Class detailsActivity() {
        return QuizDetailsActivity.class;
    }

    @Override
    protected CrypticDetailsFragment detailsFragment() {
        return quizDetailsFragment;
    }


    private class QuizListAdapter extends FilterableArrayAdapter {
        public QuizListAdapter() {
            super(application, 0);
            new LoadQuizListTask().execute();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            return SingleLineListItemView.getView(layoutInflater, R.layout.simple_list_item_1_medium_font,
                    android.R.layout.simple_list_item_activated_1, convertView, parent,
                    ((Quiz) getItem(position)).getTitle());
        }
    }


    private class LoadQuizListTask extends IndefinitelyProgressingTask<List<Quiz>> {
        public LoadQuizListTask() {
            super(getActivity(), loadingQuizes,
                    new IndefinitelyProgressingTask.OnTaskExecutionListener<List<Quiz>>() {
                        @Override
                        public List<Quiz> execute() {
                            return quizController.listQuizes();
                        }

                        @Override
                        public void onSuccess(List<Quiz> quizList) {
                            QuizListAdapter quizListAdapter = (QuizListAdapter) getListAdapter();
                            for (Quiz quiz : quizList) {
                                quizListAdapter.add(quiz);
                            }

                            quizListAdapter.sort(new Comparator<Filterable>() {
                                @Override
                                public int compare(Filterable lhs, Filterable rhs) {
                                    return ((Quiz) lhs).getTitle().compareTo(((Quiz) rhs).getTitle());
                                }
                            });

                            quizListAdapter.notifyDataSetChanged();
                        }

                        @Override
                        public void onException(Exception e) {
                            throw new RuntimeException("Error fetching quiz list from database", e);
                        }
                    }
            );
        }
    }
}
