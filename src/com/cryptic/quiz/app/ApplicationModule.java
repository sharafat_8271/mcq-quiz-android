package com.cryptic.quiz.app;

import com.cryptic.android.utils.db.AbstractDbHelper;
import com.cryptic.android.utils.photo.util.ImageUtils;
import com.cryptic.android.utils.view.ViewUtils;
import com.google.inject.AbstractModule;

/**
 * @author sharafat
 */
public class ApplicationModule extends AbstractModule {
    @Override
    protected void configure() {
        requestStaticInjection(AbstractDbHelper.class, ViewUtils.class, ImageUtils.class);
    }
}
