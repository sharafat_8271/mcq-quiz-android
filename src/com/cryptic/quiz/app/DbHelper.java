package com.cryptic.quiz.app;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Base64;
import com.cryptic.android.utils.db.AbstractDbHelper;
import com.cryptic.android.utils.db.exporterimporter.SQLiteDbSQLImporter;
import com.cryptic.quiz.R;
import com.cryptic.quiz.domain.*;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.support.ConnectionSource;

import java.io.IOException;
import java.util.List;

/**
 * @author sharafat
 */
public class DbHelper extends AbstractDbHelper {
    private static final String DB_NAME = "quiz.db";
    private static final int DB_VERSION = 1;

    public static OrmLiteSqliteOpenHelper getHelper() {
        return AbstractDbHelper.getHelper(DbHelper.class);
    }

    public DbHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION, R.raw.ormlite_config);
    }

    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
        createTables(connectionSource,
                Category.class,
                QuizCategory.class,
                Quiz.class,
                Section.class,
                Question.class,
                Answer.class);

//        insertTestData(database);
    }

    private void insertTestData(SQLiteDatabase database) {
        try {
            SQLiteDbSQLImporter.importDb(application.getResources().openRawResource(R.raw.quiz_db_test_data), database, true);

            //base64_decode BLOB type fields
            OrmLiteSqliteOpenHelper dbHelper = DbHelper.getHelper();
            RuntimeExceptionDao<Category, Integer> categoryDao = dbHelper.getRuntimeExceptionDao(Category.class);
            List<Category> categories = categoryDao.queryForAll();
            for (Category category : categories) {
                if (category.getIcon() != null) {
                    category.setIcon(Base64.decode(category.getIcon(), Base64.DEFAULT));
                    categoryDao.update(category);
                }
            }

            RuntimeExceptionDao<Question, Integer> questionDao = dbHelper.getRuntimeExceptionDao(Question.class);
            List<Question> questions = questionDao.queryForAll();
            for (Question question : questions) {
                if (question.getImageLeft() != null) {
                    question.setImageLeft(Base64.decode(question.getImageLeft(), Base64.DEFAULT));
                }
                if (question.getImageRight() != null) {
                    question.setImageRight(Base64.decode(question.getImageRight(), Base64.DEFAULT));
                }
                if (question.getImageTop() != null) {
                    question.setImageTop(Base64.decode(question.getImageTop(), Base64.DEFAULT));
                }
                if (question.getImageBottom() != null) {
                    question.setImageBottom(Base64.decode(question.getImageBottom(), Base64.DEFAULT));
                }
                questionDao.update(question);
            }

            RuntimeExceptionDao<Answer, Integer> answerDao = dbHelper.getRuntimeExceptionDao(Answer.class);
            List<Answer> answers = answerDao.queryForAll();
            for (Answer answer : answers) {
                if (answer.getImageLeft() != null) {
                    answer.setImageLeft(Base64.decode(answer.getImageLeft(), Base64.DEFAULT));
                }
                if (answer.getImageRight() != null) {
                    answer.setImageRight(Base64.decode(answer.getImageRight(), Base64.DEFAULT));
                }
                if (answer.getImageTop() != null) {
                    answer.setImageTop(Base64.decode(answer.getImageTop(), Base64.DEFAULT));
                }
                if (answer.getImageBottom() != null) {
                    answer.setImageBottom(Base64.decode(answer.getImageBottom(), Base64.DEFAULT));
                }
                answerDao.update(answer);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion) {
    }
}
