package com.cryptic.quiz.service;

import com.cryptic.quiz.app.DbHelper;
import com.cryptic.quiz.domain.Answer;
import com.cryptic.quiz.domain.Question;
import com.cryptic.quiz.domain.Quiz;
import com.cryptic.quiz.domain.Section;
import com.google.inject.Singleton;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.RuntimeExceptionDao;

import java.util.*;

/**
 * @author sharafat
 */
@Singleton
public class QuizSessionManager {
    public static final int QUIZ_MODE_LEARNING = 0;
    public static final int QUIZ_MODE_EXAM = 1;

    final RuntimeExceptionDao<Quiz, Integer> quizDao;
    final RuntimeExceptionDao<Section, Integer> sectionDao;

    /* instance variables for each quiz session */
    private Quiz quiz;
    private int quizMode;
    private Map<Section, Map<Question, List<Answer>>> sectionQuestionsMap;

    /* instance variables for each section in a quiz session */
    private Section activeSection;
    private int currentQuestionIndex;

    public QuizSessionManager() {
        OrmLiteSqliteOpenHelper dbHelper = DbHelper.getHelper();
        quizDao = dbHelper.getRuntimeExceptionDao(Quiz.class);
        sectionDao = dbHelper.getRuntimeExceptionDao(Section.class);
    }

    public void prepare(Quiz quiz, int quizMode) {
        this.quiz = quiz;
        this.quizMode = quizMode;

        quizDao.refresh(quiz);

        sectionQuestionsMap = new HashMap<Section, Map<Question, List<Answer>>>(quiz.getSections().size());
    }

    public boolean isSingleSetQuiz() {
        return quiz.getSections().size() == 1;
    }

    public List<Section> listQuizSections() {
        return new ArrayList<Section>(quiz.getSections());
    }

    public int getNoOfQuestionsInSection(Section section) {
        return getQuestionsInSection(section).size();
    }

    public int getNoOfQuestionsInActiveSection() {
        return getNoOfQuestionsInSection(activeSection);
    }

    public int getNoOfQuestionsAnsweredInSection(Section section) {
        int noOfQuestionsAnswered = 0;

        Map<Question, List<Answer>> questionListMap = sectionQuestionsMap.get(section);
        if (questionListMap == null) {
            return 0;
        }
        for (Question question : questionListMap.keySet()) {
            if (isQuestionAnswered(question)) {
                noOfQuestionsAnswered++;
            }
        }

        return noOfQuestionsAnswered;
    }

    public int getImageOfPercentageCompleted(Section section) {
        return 0;   //TODO
    }

    public int getQuizMode() {
        return quizMode;
    }

    public void setActiveSection(Section section) {
        if (!sectionQuestionsMap.containsKey(section)) {
            //TODO: replace 'false' below with actual implementation
            sectionQuestionsMap.put(section, getQuestionAnswersMapping(section, false, !quiz.shouldDisallowShuffle()));
        }

        activeSection = section;
        currentQuestionIndex = 0;
    }

    private Map<Question, List<Answer>> getQuestionAnswersMapping(Section section,
                                                                  boolean shuffleQuestions, boolean shuffleAnswers) {
        Map<Question, List<Answer>> questionAnswersMap = new LinkedHashMap<Question, List<Answer>>();

        List<Question> questions = getQuestionsInSection(section);
        if (shuffleQuestions) {
            Collections.shuffle(questions);
        }

        for (Question question : questions) {
            List<Answer> answers = new ArrayList<Answer>(getAnswers(question));
            if (shuffleAnswers) {
                Collections.shuffle(answers);
            }

            questionAnswersMap.put(question, answers);
        }

        return questionAnswersMap;
    }

    private List<Question> getQuestionsInSection(Section section) {
        List<Question> questions = new ArrayList<Question>();

        if (sectionQuestionsMap.containsKey(section)) {
            questions.addAll(sectionQuestionsMap.get(section).keySet());
        } else {
            try {
                //Check if accessing section.getQuestions() throws exception
                //as lazy collections cannot be used if they have been deserialized
                section.getQuestions().size();
            } catch (IllegalStateException e) {
                sectionDao.refresh(section);
            } finally {
                questions.addAll(section.getQuestions());
            }
        }

        return questions;
    }

    public Question getQuestion() {
        return getQuestionsInSection(activeSection).get(currentQuestionIndex);
    }

    public List<Answer> getAnswers() {
        return getAnswers(getQuestion());
    }

    private List<Answer> getAnswers(Question question) {
        List<Answer> answers = null;
        try {
            answers = sectionQuestionsMap.get(question.getSection()).get(question);
        } catch (NullPointerException ignore) {
        }

        return answers != null ? answers : new ArrayList<Answer>(question.getAnswers());
    }

    public boolean canChooseMultipleAnswers() {
        Question question = getQuestion();

        if (question.canChooseMultipleAnswers() == null) {
            question.setCanChooseMultipleAnswers(quiz.shouldForceMultipleChoice() || hasMultipleCorrectAnswers());
        }

        return question.canChooseMultipleAnswers();
    }

    private boolean hasMultipleCorrectAnswers() {
        boolean correctAnswerAlreadyFound = false;

        for (Answer answer : getAnswers()) {
            if (answer.isCorrect()) {
                if (correctAnswerAlreadyFound) {
                    return true;
                }
                correctAnswerAlreadyFound = true;
            }
        }

        return false;
    }

    public boolean isFirstQuestionInActiveSection() {
        return currentQuestionIndex == 0;
    }

    public boolean isLastQuestionInActiveSection() {
        return currentQuestionIndex == getQuestionsInSection(activeSection).size() - 1;
    }

    public boolean isQuestionAnswered() {
        return isQuestionAnswered(getQuestion());
    }

    private boolean isQuestionAnswered(Question question) {
        if (question.isAnswered() == null) {
            for (Answer answer : getAnswers()) {
                if (answer.isMarked()) {
                    question.setAnswered(true);
                    break;
                }
            }
            question.setAnswered(false);
        }

        return question.isAnswered();
    }

    public void answerMarked(Integer... answerIndices) {
        resetAnswerMarkings();

        for (Integer answerIndex : answerIndices) {
            getAnswers().get(answerIndex).setMarked(true);
        }

        if (answerIndices.length > 0
                || quizMode == QUIZ_MODE_LEARNING) { //This is for the case where the user clicks "Check Answer" without
                                                     //selecting any answer. It's only applicable to Learning Mode.
            getQuestion().setAnswered(true);
        }
    }

    private void resetAnswerMarkings() {
        for (Answer answer : getAnswers()) {
            answer.setMarked(false);
        }
    }

    public Map<Integer, Boolean> getAnswerPositionsWithCorrectness() {
        Map<Integer, Boolean> answerPositionsCorrectnessMap = new HashMap<Integer, Boolean>();

        List<Answer> answers = getAnswers();
        for (int i = 0; i < answers.size(); i++) {
            Answer answer = answers.get(i);
            if (answer.isMarked() && !answer.isCorrect()) {
                answerPositionsCorrectnessMap.put(i, false);
            } else if (answer.isCorrect()) {
                answerPositionsCorrectnessMap.put(i, true);
            }
        }

        return answerPositionsCorrectnessMap;
    }

    public void next() {
        currentQuestionIndex++;
    }

    public void previous() {
        currentQuestionIndex--;
    }

    public int getCurrentQuestionNo() {
        return currentQuestionIndex + 1;
    }

    public String getQuizTitle() {
        return quiz.getTitle();
    }

    public Section getActiveSection() {
        return activeSection;
    }

    public String getActiveSectionHeading() {
        return activeSection.getHeading();
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        DbHelper.release();
    }
}
