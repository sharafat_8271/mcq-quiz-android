package com.cryptic.quiz.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import com.cryptic.android.utils.view.ViewUtils;
import com.cryptic.quiz.R;
import com.cryptic.quiz.fragment.QuizFragment;
import com.cryptic.quiz.fragment.QuizSectionListFragment;
import com.cryptic.quiz.service.QuizSessionManager;
import com.google.inject.Inject;
import roboguice.activity.RoboFragmentActivity;
import roboguice.inject.ContentView;
import roboguice.inject.InjectView;

import javax.annotation.Nullable;

/**
 * @author sharafat
 */
@ContentView(R.layout.list)
public class QuizSectionListActivity extends RoboFragmentActivity {
    @Inject
    private QuizSessionManager quizSessionManager;
    @Inject
    private QuizSectionListFragment quizSectionListFragment;
    @Inject
    private QuizFragment quizFragment;

    @InjectView(R.id.details_container)
    @Nullable
    private LinearLayout detailsContainer;

    @InjectView(R.id.filter_input)
    private EditText filterInput;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (quizSessionManager.isSingleSetQuiz()) {
            quizSectionListFragment = null;
            startActivity(new Intent(this, QuizActivity.class));
        } else {
            filterInput.setVisibility(View.GONE);
            ViewUtils.addListFragmentToLayout(this, detailsContainer, quizSectionListFragment, quizFragment);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == QuizFragment.RESULT_FINISH) {
            finish();
        }
    }
}
