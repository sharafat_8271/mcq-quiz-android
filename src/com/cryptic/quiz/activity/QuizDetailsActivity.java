package com.cryptic.quiz.activity;

import com.cryptic.android.utils.view.activity.CrypticDetailsActivity;
import com.cryptic.android.utils.view.fragment.CrypticDetailsFragment;
import com.cryptic.quiz.fragment.QuizDetailsFragment;
import com.google.inject.Inject;

/**
 * @author sharafat
 */
public class QuizDetailsActivity extends CrypticDetailsActivity {
    @Inject
    private QuizDetailsFragment quizDetailsFragment;

    @Override
    protected CrypticDetailsFragment detailsFragment() {
        return quizDetailsFragment;
    }
}
