package com.cryptic.quiz.activity;

import android.os.Bundle;
import android.widget.LinearLayout;
import com.cryptic.android.utils.view.ViewUtils;
import com.cryptic.quiz.R;
import com.cryptic.quiz.fragment.QuizDetailsFragment;
import com.cryptic.quiz.fragment.QuizListFragment;
import com.google.inject.Inject;
import roboguice.activity.RoboFragmentActivity;
import roboguice.inject.ContentView;
import roboguice.inject.InjectView;

import javax.annotation.Nullable;

@ContentView(R.layout.list)
public class QuizListActivity extends RoboFragmentActivity {
    @Inject
    private QuizListFragment quizListFragment;
    @Inject
    private QuizDetailsFragment quizDetailsFragment;

    @InjectView(R.id.details_container)
    @Nullable
    private LinearLayout detailsContainer;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ViewUtils.addListFragmentToLayout(this, detailsContainer, quizListFragment, quizDetailsFragment);
    }
}
