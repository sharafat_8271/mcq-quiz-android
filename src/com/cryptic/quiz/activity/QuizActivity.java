package com.cryptic.quiz.activity;

import com.cryptic.android.utils.view.activity.CrypticDetailsActivity;
import com.cryptic.android.utils.view.fragment.CrypticDetailsFragment;
import com.cryptic.quiz.fragment.QuizFragment;
import com.google.inject.Inject;

/**
 * @author sharafat
 */
public class QuizActivity extends CrypticDetailsActivity {
    @Inject
    private QuizFragment quizFragment;

    @Override
    protected CrypticDetailsFragment detailsFragment() {
        return quizFragment;
    }
}
