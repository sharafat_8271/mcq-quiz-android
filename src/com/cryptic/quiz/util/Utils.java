package com.cryptic.quiz.util;

import android.app.Activity;
import com.cryptic.android.utils.view.ViewUtils;
import com.cryptic.quiz.activity.QuizListActivity;

/**
 * @author sharafat
 */
public class Utils {
    public static Class getHomeActivity() {
        return QuizListActivity.class;
    }

    public static void startHomeActivity(Activity fromActivity) {
        ViewUtils.startHomeActivity(getHomeActivity(), fromActivity);
    }
}
