package com.cryptic.quiz.controller;

import com.cryptic.quiz.app.DbHelper;
import com.cryptic.quiz.domain.Quiz;
import com.google.inject.Singleton;
import com.j256.ormlite.dao.RuntimeExceptionDao;

import java.sql.SQLException;
import java.util.List;

/**
 * @author sharafat
 */
@Singleton
public class QuizController {
    private final RuntimeExceptionDao<Quiz, Integer> quizDao;

    public QuizController() {
        quizDao = DbHelper.getHelper().getRuntimeExceptionDao(Quiz.class);
    }

    public List<Quiz> listQuizes() {
        return quizDao.queryForAll();
    }

    public int getNoOfQuestionsInQuiz(Quiz quiz) {
        String sql = "select count(id)" +
                " from question" +
                " where section_id in (" +
                " select id from section where quiz_id = " + quiz.getId()
                + ")";
        try {
            return Integer.parseInt(quizDao.queryRaw(sql).getResults().get(0)[0]);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        DbHelper.release();
    }
}
