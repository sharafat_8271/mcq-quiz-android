package com.cryptic.quiz.domain;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;

/**
 * @author sharafat
 */
@DatabaseTable
public class Category implements Serializable {
    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField(canBeNull = false)
    private String name;

    @DatabaseField(dataType = DataType.BYTE_ARRAY)
    private byte[] icon;

    @DatabaseField(foreign = true, index = true)
    private Category parentCategory;

    @ForeignCollectionField(orderColumnName = "name")
    private Collection<Category> childCategories;

    @ForeignCollectionField
    private Collection<QuizCategory> quizCategories;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public byte[] getIcon() {
        return icon;
    }

    public void setIcon(byte[] icon) {
        this.icon = icon;
    }

    public Category getParentCategory() {
        return parentCategory;
    }

    public void setParentCategory(Category parentCategory) {
        this.parentCategory = parentCategory;
    }

    public Collection<Category> getChildCategories() {
        return childCategories;
    }

    public void setChildCategories(Collection<Category> childCategories) {
        this.childCategories = childCategories;
    }

    public Collection<QuizCategory> getQuizCategories() {
        return quizCategories;
    }

    public void setQuizCategories(Collection<QuizCategory> quizCategories) {
        this.quizCategories = quizCategories;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Category category = (Category) o;

        if (id != category.id) return false;
        if (childCategories != null ? !childCategories.equals(category.childCategories) : category.childCategories != null)
            return false;
        if (!Arrays.equals(icon, category.icon)) return false;
        if (name != null ? !name.equals(category.name) : category.name != null) return false;
        if (parentCategory != null ? !parentCategory.equals(category.parentCategory) : category.parentCategory != null)
            return false;
        if (quizCategories != null ? !quizCategories.equals(category.quizCategories) : category.quizCategories != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (icon != null ? Arrays.hashCode(icon) : 0);
        result = 31 * result + (parentCategory != null ? parentCategory.hashCode() : 0);
        result = 31 * result + (childCategories != null ? childCategories.hashCode() : 0);
        result = 31 * result + (quizCategories != null ? quizCategories.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Category{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", iconLength=" + (icon == null ? 0 : icon.length) +
                ", parentCategory=" + parentCategory +
                ", noOfChildCategories=" + (childCategories == null ? 0 : childCategories.size()) +
                ", noOfQuizCategories=" + (quizCategories == null ? 0 : quizCategories.size()) +
                '}';
    }
}
