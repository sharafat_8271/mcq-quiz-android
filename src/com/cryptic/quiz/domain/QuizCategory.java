package com.cryptic.quiz.domain;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

/**
 * @author sharafat
 */
@DatabaseTable
public class QuizCategory implements Serializable {
    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField(canBeNull = false, foreign = true, foreignAutoRefresh = true, index = true,
            columnDefinition = "integer references quiz(id) on delete cascade")
    private Quiz quiz;

    @DatabaseField(canBeNull = false, foreign = true, foreignAutoRefresh = true, index = true,
            columnDefinition = "integer references category(id) on delete cascade")
    private Category category;

    public QuizCategory() {
        //for ormlite
    }

    public QuizCategory(Quiz quiz, Category category) {
        this.quiz = quiz;
        this.category = category;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Quiz getQuiz() {
        return quiz;
    }

    public void setQuiz(Quiz quiz) {
        this.quiz = quiz;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        QuizCategory that = (QuizCategory) o;

        if (id != that.id) return false;
        if (category != null ? !category.equals(that.category) : that.category != null) return false;
        if (quiz != null ? !quiz.equals(that.quiz) : that.quiz != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (quiz != null ? quiz.hashCode() : 0);
        result = 31 * result + (category != null ? category.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "QuizCategory{" +
                "id=" + id +
                ", quiz=" + quiz +
                ", category=" + category +
                '}';
    }
}
