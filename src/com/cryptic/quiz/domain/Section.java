package com.cryptic.quiz.domain;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;
import java.util.Collection;

/**
 * @author sharafat
 */
@DatabaseTable
public class Section implements Serializable {
    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField(canBeNull = false, foreign = true, index = true,
            columnDefinition = "integer references quiz(id) on delete cascade")
    private Quiz quiz;

    @DatabaseField(canBeNull = false)
    private String heading;

    @ForeignCollectionField(orderColumnName = "id")
    private Collection<Question> questions;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Quiz getQuiz() {
        return quiz;
    }

    public void setQuiz(Quiz quiz) {
        this.quiz = quiz;
    }

    public String getHeading() {
        return heading;
    }

    public void setHeading(String heading) {
        this.heading = heading;
    }

    public Collection<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(Collection<Question> questions) {
        this.questions = questions;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Section section = (Section) o;

        //TODO: verify whether this is really needed
        if (id == section.id && id != 0) {
            return true;
        }

        if (id != section.id) return false;
        if (heading != null ? !heading.equals(section.heading) : section.heading != null) return false;
        if (questions != null ? !questions.equals(section.questions) : section.questions != null) return false;
        if (quiz != null ? !quiz.equals(section.quiz) : section.quiz != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        //TODO: verify whether this is really needed
        if (id != 0) {
            return id;
        }

        int result = id;
        result = 31 * result + (quiz != null ? quiz.hashCode() : 0);
        result = 31 * result + (heading != null ? heading.hashCode() : 0);
        result = 31 * result + (questions != null ? questions.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Section{" +
                "id=" + id +
                ", quiz=" + quiz +
                ", heading='" + heading + '\'' +
                ", noOfQuestions=" + (questions == null ? 0 : questions.size()) +
                '}';
    }
}
