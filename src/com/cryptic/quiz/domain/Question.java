package com.cryptic.quiz.domain;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;

/**
 * @author sharafat
 */
@DatabaseTable
public class Question implements Serializable {
    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField(canBeNull = false, foreign = true, index = true,
            columnDefinition = "integer references section(id) on delete cascade")
    private Section section;

    @DatabaseField
    private String text;

    @DatabaseField(dataType = DataType.BYTE_ARRAY)
    private byte[] imageTop;

    @DatabaseField(dataType = DataType.BYTE_ARRAY)
    private byte[] imageRight;

    @DatabaseField(dataType = DataType.BYTE_ARRAY)
    private byte[] imageBottom;

    @DatabaseField(dataType = DataType.BYTE_ARRAY)
    private byte[] imageLeft;

    @DatabaseField
    private String explanation;

    @ForeignCollectionField(orderColumnName = "id")
    private Collection<Answer> answers;

    private Boolean answered;

    private Boolean canChooseMultipleAnswers;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Section getSection() {
        return section;
    }

    public void setSection(Section section) {
        this.section = section;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public byte[] getImageTop() {
        return imageTop;
    }

    public void setImageTop(byte[] imageTop) {
        this.imageTop = imageTop;
    }

    public byte[] getImageRight() {
        return imageRight;
    }

    public void setImageRight(byte[] imageRight) {
        this.imageRight = imageRight;
    }

    public byte[] getImageBottom() {
        return imageBottom;
    }

    public void setImageBottom(byte[] imageBottom) {
        this.imageBottom = imageBottom;
    }

    public byte[] getImageLeft() {
        return imageLeft;
    }

    public void setImageLeft(byte[] imageLeft) {
        this.imageLeft = imageLeft;
    }

    public String getExplanation() {
        return explanation;
    }

    public void setExplanation(String explanation) {
        this.explanation = explanation;
    }

    public Collection<Answer> getAnswers() {
        return answers;
    }

    public void setAnswers(Collection<Answer> answers) {
        this.answers = answers;
    }

    public Boolean isAnswered() {
        return answered;
    }

    public Boolean canChooseMultipleAnswers() {
        return canChooseMultipleAnswers;
    }

    public void setCanChooseMultipleAnswers(Boolean canChooseMultipleAnswers) {
        this.canChooseMultipleAnswers = canChooseMultipleAnswers;
    }

    public void setAnswered(Boolean answered) {
        this.answered = answered;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Question question = (Question) o;

        //TODO: verify whether this is really needed
        if (id == question.id && id != 0) {
            return true;
        }

        if (id != question.id) return false;
        if (answers != null ? !answers.equals(question.answers) : question.answers != null) return false;
        if (explanation != null ? !explanation.equals(question.explanation) : question.explanation != null)
            return false;
        if (!Arrays.equals(imageBottom, question.imageBottom)) return false;
        if (!Arrays.equals(imageLeft, question.imageLeft)) return false;
        if (!Arrays.equals(imageRight, question.imageRight)) return false;
        if (!Arrays.equals(imageTop, question.imageTop)) return false;
        if (section != null ? !section.equals(question.section) : question.section != null) return false;
        if (text != null ? !text.equals(question.text) : question.text != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        //TODO: verify whether this is really needed
        if (id != 0) {
            return id;
        }

        int result = id;
        result = 31 * result + (section != null ? section.hashCode() : 0);
        result = 31 * result + (text != null ? text.hashCode() : 0);
        result = 31 * result + (imageTop != null ? Arrays.hashCode(imageTop) : 0);
        result = 31 * result + (imageRight != null ? Arrays.hashCode(imageRight) : 0);
        result = 31 * result + (imageBottom != null ? Arrays.hashCode(imageBottom) : 0);
        result = 31 * result + (imageLeft != null ? Arrays.hashCode(imageLeft) : 0);
        result = 31 * result + (explanation != null ? explanation.hashCode() : 0);
        result = 31 * result + (answers != null ? answers.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Question{" +
                "id=" + id +
                ", section=" + section +
                ", text='" + text + '\'' +
                ", imageTopLength=" + (imageTop == null ? 0 : imageTop.length) +
                ", imageRightLength=" + (imageRight == null ? 0 : imageRight.length) +
                ", imageBottomLength=" + (imageBottom == null ? 0 : imageBottom.length) +
                ", imageLeftLength=" + (imageLeft == null ? 0 : imageLeft.length) +
                ", explanation='" + explanation + '\'' +
                ", noOfAnswers=" + (answers == null ? 0 : answers.size()) +
                '}';
    }
}
