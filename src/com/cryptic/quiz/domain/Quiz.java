package com.cryptic.quiz.domain;

import com.cryptic.android.utils.adapter.Filterable;
import com.cryptic.android.utils.datetime.TimeWithoutDate;
import com.cryptic.android.utils.db.persisters.TimeWithoutDatePersister;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

/**
 * @author sharafat
 */
@DatabaseTable
public class Quiz implements Serializable, Filterable {
    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField(canBeNull = false)
    private String title;

    @DatabaseField
    private String details;

    @DatabaseField(canBeNull = false)
    private Date lastUpdated = new Date();

    @DatabaseField(canBeNull = false)
    private boolean forceMultipleChoice;

    @DatabaseField(canBeNull = false)
    private boolean disallowShuffle;

    @DatabaseField(persisterClass = TimeWithoutDatePersister.class)
    private TimeWithoutDate examTime;

    @DatabaseField(canBeNull = false)
    private boolean learningModeCompleted;

    @DatabaseField(canBeNull = false)
    private boolean examModeCompleted;

    @ForeignCollectionField
    private Collection<QuizCategory> quizCategories;

    @ForeignCollectionField(orderColumnName = "id")
    private Collection<Section> sections;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public boolean shouldForceMultipleChoice() {
        return forceMultipleChoice;
    }

    public void setForceMultipleChoice(boolean forceMultipleChoice) {
        this.forceMultipleChoice = forceMultipleChoice;
    }

    public boolean shouldDisallowShuffle() {
        return disallowShuffle;
    }

    public void setDisallowShuffle(boolean disallowShuffle) {
        this.disallowShuffle = disallowShuffle;
    }

    public TimeWithoutDate getExamTime() {
        return examTime;
    }

    public void setExamTime(TimeWithoutDate examTime) {
        this.examTime = examTime;
    }

    public boolean isLearningModeCompleted() {
        return learningModeCompleted;
    }

    public void setLearningModeCompleted(boolean learningModeCompleted) {
        this.learningModeCompleted = learningModeCompleted;
    }

    public boolean isExamModeCompleted() {
        return examModeCompleted;
    }

    public void setExamModeCompleted(boolean examModeCompleted) {
        this.examModeCompleted = examModeCompleted;
    }

    public Collection<QuizCategory> getQuizCategories() {
        return quizCategories;
    }

    public void setQuizCategories(Collection<QuizCategory> quizCategories) {
        this.quizCategories = quizCategories;
    }

    public Collection<Section> getSections() {
        return sections;
    }

    public void setSections(Collection<Section> sections) {
        this.sections = sections;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Quiz quiz = (Quiz) o;

        if (disallowShuffle != quiz.disallowShuffle) return false;
        if (examModeCompleted != quiz.examModeCompleted) return false;
        if (forceMultipleChoice != quiz.forceMultipleChoice) return false;
        if (id != quiz.id) return false;
        if (learningModeCompleted != quiz.learningModeCompleted) return false;
        if (details != null ? !details.equals(quiz.details) : quiz.details != null) return false;
        if (examTime != null ? !examTime.equals(quiz.examTime) : quiz.examTime != null) return false;
        if (lastUpdated != null ? !lastUpdated.equals(quiz.lastUpdated) : quiz.lastUpdated != null) return false;
        if (quizCategories != null ? !quizCategories.equals(quiz.quizCategories) : quiz.quizCategories != null)
            return false;
        if (sections != null ? !sections.equals(quiz.sections) : quiz.sections != null) return false;
        if (title != null ? !title.equals(quiz.title) : quiz.title != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (details != null ? details.hashCode() : 0);
        result = 31 * result + (lastUpdated != null ? lastUpdated.hashCode() : 0);
        result = 31 * result + (forceMultipleChoice ? 1 : 0);
        result = 31 * result + (disallowShuffle ? 1 : 0);
        result = 31 * result + (examTime != null ? examTime.hashCode() : 0);
        result = 31 * result + (learningModeCompleted ? 1 : 0);
        result = 31 * result + (examModeCompleted ? 1 : 0);
        result = 31 * result + (quizCategories != null ? quizCategories.hashCode() : 0);
        result = 31 * result + (sections != null ? sections.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Quiz{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", details='" + details + '\'' +
                ", lastUpdated=" + lastUpdated +
                ", forceMultipleChoice=" + forceMultipleChoice +
                ", disallowShuffle=" + disallowShuffle +
                ", examTime=" + examTime +
                ", learningModeCompleted=" + learningModeCompleted +
                ", examModeCompleted=" + examModeCompleted +
                ", noOfQuizCategories=" + (quizCategories == null ? 0 : quizCategories.size()) +
                ", noOfSections=" + (sections == null ? 0 : sections.size()) +
                '}';
    }

    @Override
    public String getFilterableText() {
        return title;
    }
}
