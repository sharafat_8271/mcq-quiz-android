package com.cryptic.quiz.domain;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;
import java.util.Arrays;

/**
 * @author sharafat
 */
@DatabaseTable
public class Answer implements Serializable {
    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField(canBeNull = false, foreign = true, index = true,
            columnDefinition = "integer references question(id) on delete cascade")
    private Question question;

    @DatabaseField
    private String text;

    @DatabaseField(dataType = DataType.BYTE_ARRAY)
    private byte[] imageTop;

    @DatabaseField(dataType = DataType.BYTE_ARRAY)
    private byte[] imageRight;

    @DatabaseField(dataType = DataType.BYTE_ARRAY)
    private byte[] imageBottom;

    @DatabaseField(dataType = DataType.BYTE_ARRAY)
    private byte[] imageLeft;

    @DatabaseField(canBeNull = false)
    private boolean correct;

    private boolean marked;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public byte[] getImageTop() {
        return imageTop;
    }

    public void setImageTop(byte[] imageTop) {
        this.imageTop = imageTop;
    }

    public byte[] getImageRight() {
        return imageRight;
    }

    public void setImageRight(byte[] imageRight) {
        this.imageRight = imageRight;
    }

    public byte[] getImageBottom() {
        return imageBottom;
    }

    public void setImageBottom(byte[] imageBottom) {
        this.imageBottom = imageBottom;
    }

    public byte[] getImageLeft() {
        return imageLeft;
    }

    public void setImageLeft(byte[] imageLeft) {
        this.imageLeft = imageLeft;
    }

    public boolean isCorrect() {
        return correct;
    }

    public void setCorrect(boolean correct) {
        this.correct = correct;
    }

    public boolean isMarked() {
        return marked;
    }

    public void setMarked(boolean marked) {
        this.marked = marked;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Answer answer = (Answer) o;

        if (correct != answer.correct) return false;
        if (id != answer.id) return false;
        if (marked != answer.marked) return false;
        if (!Arrays.equals(imageBottom, answer.imageBottom)) return false;
        if (!Arrays.equals(imageLeft, answer.imageLeft)) return false;
        if (!Arrays.equals(imageRight, answer.imageRight)) return false;
        if (!Arrays.equals(imageTop, answer.imageTop)) return false;
        if (question != null ? !question.equals(answer.question) : answer.question != null) return false;
        if (text != null ? !text.equals(answer.text) : answer.text != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (question != null ? question.hashCode() : 0);
        result = 31 * result + (text != null ? text.hashCode() : 0);
        result = 31 * result + (imageTop != null ? Arrays.hashCode(imageTop) : 0);
        result = 31 * result + (imageRight != null ? Arrays.hashCode(imageRight) : 0);
        result = 31 * result + (imageBottom != null ? Arrays.hashCode(imageBottom) : 0);
        result = 31 * result + (imageLeft != null ? Arrays.hashCode(imageLeft) : 0);
        result = 31 * result + (correct ? 1 : 0);
        result = 31 * result + (marked ? 1 : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Answer{" +
                "id=" + id +
                ", question=" + question +
                ", text='" + text + '\'' +
                ", imageTopLength=" + (imageTop == null ? 0 : imageTop.length) +
                ", imageRightLength=" + (imageRight == null ? 0 : imageRight.length) +
                ", imageBottomLength=" + (imageBottom == null ? 0 : imageBottom.length) +
                ", imageLeftLength=" + (imageLeft == null ? 0 : imageLeft.length) +
                ", correct=" + correct +
                ", marked=" + marked +
                '}';
    }
}
