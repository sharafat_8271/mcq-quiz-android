# README #

MCQ Quiz is a generic MCQ quiz platform for Android. Any type of quiz can be viewed in this app. Features include:

* Learning Mode: Correct answer of a question is shown after answering each question
* Exam Mode: Correct answers to all the questions in an exam is shown after the user finishes answering all the questions
* Questions can be single-choice or multiple-choice
* Images can be placed on four sides (top, bottom, right left) of a question or answer
* Different sections can be placed for a quiz set

### Screenshots ###

![MCQ Quiz Screenshots](https://bytebucket.org/sharafat_8271/mcq-quiz-android/raw/edabfaa142a1f021f49d556bb142267329769385/Screenshots.png)